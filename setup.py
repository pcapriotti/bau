from setuptools import setup

setup(name='bau',
      version='0.1',
      description='A python-based build system',
      url='https://gitlab.com/pcapriotti/bau',
      author='Paolo Capriotti',
      author_email='paolo@capriotti.io',
      license='MIT',
      packages=['bau'],
      scripts=['bin/bau'])
