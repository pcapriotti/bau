# [bau](https://pcapriotti.gitlab.io/bau)

[![pipeline status](https://gitlab.com/pcapriotti/bau/badges/master/pipeline.svg)](https://gitlab.com/pcapriotti/bau/-/commits/master)
[![pipy](https://img.shields.io/pypi/v/bau)](https://pypi.org/project/bau)

---

# bau

A build system with a focus on reproducibility and correctness.

## Introduction

The basic principle of `bau` is that a "building process" can be represented
simply as a script where some of the intermediate computations or generated
files can be cached.  Building from any state should produce exactly the same
artifacts as building from scratch.

The fundamental abstraction in `bau` is that of a *rule*. A rule is essentially
a memoized python function. This means that the second time it is called with
the same arguments, it returns the previously computed result without doing any
work. Furthermore, this holds across distinct invocations of a script.

Most steps of a build process, however, do not only consist of pure
computations. Often, external programs like compilers are spawned, and
intermediate files are generated. To make sure that such situations are handled
correctly, rules define the set of files that can potentially affect their
behaviour. These files include both the *inputs* and the *outputs*. Whenever
any of such files change, the previously cached results are considered
invalidated, and the rule is run again.

To achieve persistence of cached information across different runs, `bau` uses
an sqlite database, which is stored in the build directory.
